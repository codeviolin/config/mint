############################# Optional Demos #############################

# Here are some demos of the concepts behind the design decisions used in
# this configuration. They are best explored directly from the bashrc
# file to show how they apply. Just remove them by deleting everything
# until the end of file in your copy if you like.

####### Functions Never Invoke a Subshell, Even in Subshell Context ######

# Note that most functions are exported allowing them to be used from any
# interactive command line (replacing what might otherwise be created as
# a standalone #!/bin/bash script). This allows the function to be used in
# every way exactly as if it had been written as a script (at the minor
# cost of polluting ENV with BASH_FUNCTION* values).

# Using this command function approach is the fastest possible way to
# execute since there is literally zero startup parsing time (which is
# done when you log in instead). As a result there is never a subshell
# created to call these even when used in subshell syntax $(myfunc)

# The only cavaet is that use of $$ will always be the same value so do
# not use it for any type of unique file naming from other functions here.
# Use tstamp or something instead.

# To see the proof of this, uncomment the functions below to produce
# something like the following output:

#   [rob@sk config]$ echo $$; _demo-no-subshell-for-func 
#   12578
#   PID of calling function: 12578
#   Calling the script as a subshell:
#   PID of an actual subshelled script: 29275
#   PID of an actual subshelled script: 29276
#   Calling the function as a subshell:
#   PID of Function (would be subshelled if not function): 12578
#   PID of Function (would be subshelled if not function): 12578

_demo-no-subshell-for-func () {
  tell '*Making temporary script:* `'$tmpname'`'
  declare buf
	declare tmpname="/tmp/_demo-no-subshell-for-func.$(tstamp)"

  echo "PID of calling function: $$"

  echo '#!/bin/bash
		echo "PID of an actual subshelled script: $$"' > "$tmpname"
	chmod +x "$tmpname"

  tell "*Calling the script as a subshell:*"
  ($tmpname);         # without capture
  echo "$($tmpname)"  # with capture
  rm "$tmpname"

  tell "*Calling the function as a subshell:*"
  (_func-as-cmd)
  echo "$(_func-as-cmd)"

} && export -f _demo-no-subshell-for-func

_func-as-cmd () {
  declare pid=$$
  echo "PID of Function (would be subshelled if not function): $pid"
} && export -f _func-as-cmd

